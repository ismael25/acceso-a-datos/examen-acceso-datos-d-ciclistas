<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
     public function actionConsultaorm(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->select("SUM(kms) AS Kms_Totales")
            ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Kms_Totales'],
            "titulo"=>"Consulta 2 ",
            "enunciado"=>"El numero Total de kms recorridos por los ciclistas",
            "razonamiento"=>" Para llegar a la consulta he sumado todos los kms de la tabla etapa "
        ]);
    }
    public function actionConsultadao(){
        

        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT  dorsal ,nombre FROM ciclista 
            WHERE dorsal IN (SELECT dorsal   FROM lleva 
                  WHERE código IN (SELECT código   FROM maillot  WHERE color != 'rosa')) 
            AND dorsal IN (SELECT dorsal FROM etapa )",
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 1",
            "enunciado"=>"Los nombre y los dorsales de los ciclista que habiendo ganado etapas, nuncan han llevado el maillot rosa",
            "razonamiento"=>"1º los maillot que no son rosa , 2º Luego cogemos los dorsales que coincida con lo anterior , 3º y añadimos un y que coincidan con los que han ganado etapas"
        ]);
    }
}
